package handlers

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

// Ctrls is control channels map
type Ctrls map[string]chan bool

func CtrlCreate() Ctrls {
	ctrl := make(map[string]chan bool)

	ctrl["ok"] = make(chan bool, 1)
	ctrl["stop"] = make(chan bool, 1)
	ctrl["restart"] = make(chan bool, 1)

	return ctrl
}

func MainHandler(w http.ResponseWriter, r *http.Request) {

	fmt.Println()

	if r.Method == "GET" {
		fmt.Println("GET")

		if len(r.URL.Query()) > 0 {

			for key, val := range r.URL.Query() {
				fmt.Println(key, val)
			}
		} else {
			fmt.Println("GET Nothing")
		}
	}

	if r.Method == "POST" {
		fmt.Println("POST")

		err := r.ParseForm()
		if err != nil {
			fmt.Println("error: form parse error")
			fmt.Println(err.Error())
			return
		}

		if len(r.PostForm) > 0 {

			for key, val := range r.PostForm {
				fmt.Println(key, val)
			}
		} else {
			fmt.Println("POST Nothing")
		}
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func WSHandler(w http.ResponseWriter, r *http.Request) {

	upgrader := websocket.Upgrader{}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("ws error:", err)
		return
	}
	defer conn.Close()

	for {

		mt, message, err := conn.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break
		}

		fmt.Println("ws received:", string(message))

		reply := "meow :3"
		message = []byte(reply)

		err = conn.WriteMessage(mt, message)
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println("ws send:", string(message))
	}
}

// AsyncHandler tests async ajax request
func AsyncHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println()

	if r.Method == "GET" {
		fmt.Println("ASYNC GET")

		if len(r.URL.Query()) > 0 {

			for key, val := range r.URL.Query() {
				fmt.Println(key, val)
			}
		} else {
			fmt.Println("ASYNC GET Nothing")
		}
	}

	if r.Method == "POST" {
		fmt.Println("ASYNC POST")

		err := r.ParseForm()
		if err != nil {
			fmt.Println("error: form parse error")
			fmt.Println(err.Error())
			return
		}

		if len(r.PostForm) > 0 {

			for key, val := range r.PostForm {
				fmt.Println(key, val)
				res := "key: " + key + "\n"
				res = res + "val: " + strings.Join(val, " ") + "\n"
				w.Write([]byte(res))
			}

		} else {
			fmt.Println("ASYNC POST Nothing")
		}

	}
}

func RestartHandler(ctrl Ctrls) {
	http.HandleFunc("/restart", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Restart...")

		http.Redirect(w, r, "/", http.StatusSeeOther)
		ctrl["restart"] <- true
	})
}

func ShutdownHandler(ctrl Ctrls) {
	http.HandleFunc("/shutdown", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Shutdown...")

		http.Redirect(w, r, "/shutdown.html", http.StatusSeeOther)

		ctrl["stop"] <- true
	})
}
