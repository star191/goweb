var ws = null;

function wsHandler() {
    let address = "ws://localhost:8080/ws";
    let ws = new WebSocket(address);

    
    ws.onopen = function() {
        console.log("ws connected");

        // send data to server
        ws.send("test");
        
    };

    ws.onclose = function(event) {
        if (event.wasClean) {
            console.log("ws close clearly");
        } else {
            console.log("ws disconnect");
        }
        //console.log('Code: ' + event.code + ' reason: ' + event.reason);
    };

    ws.onmessage = function(event) {
        console.log("ws received: " + event.data);

        // close connection after receive from server
        ws.close();
    };

    ws.onerror = function(error) {
        console.log("Error: " + error.message);
    };
}

function asyncGETHandler() {
    console.log("asyncGETHandler()")

    let xhr = new XMLHttpRequest();

    xhr.open('GET', '/async?meow=20', true);

    //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("meow=20&murr=42");
}

function asyncPOSTHandler() {
    console.log("asyncPOSTHandler()")

    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/async', true);

    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.send("meow=5&murr=6");

    xhr.onreadystatechange = function() {
        if (this.readyState != 4) return;

        if (this.status != 200) {
            console.log('async POST error');
            console.log('ошибка: ' + (this.status ? this.statusText : 'запрос не удался'));
            return;
        }

        console.log('Response:');
        console.log(this.responseText);
    }
}